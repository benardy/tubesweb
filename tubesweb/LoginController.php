<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function login(){
    	return view('penggajian.login');
    }

    public function getLogin(Request $request){
    	if(Auth::attempt([
    		'username' => $request->username,
    		'password' => $request->password

    	])) {
    		return redirect('penggajian.index');
    	}else {
    		return 'username atau password salah';
    	}
    }
}
